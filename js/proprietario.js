$(document).ready(function () {
    var table = $('#proprietario').DataTable({
        "paging": false,
        "ordering": false,
        "info": false,
        "sAjaxSource": "http://localhost:8080/proprietarios",
        "sAjaxDataProp": "",
        "language": {
            "search": "Pesquisar",
            "searchPlaceholder": "Pesquisar na Tabela"
        },

        "crossDomain": true,
        "order": [[0, "asc"]],
        "aoColumns": [
            { "mData": "nomeEmpresa" },
            { "mData": "nome" },
            { "mData": "email" },
            { "mData": "telefone" },
            {
                "mData": null,
                "bSortable": false,
                "mRender": function (data, type, full) {
                    return '<a class="btn btn-info btn-sm" href=#/' + full[0] + '>' + 'Edit' + '</a>';
                }
            }
        ]
    })
});
